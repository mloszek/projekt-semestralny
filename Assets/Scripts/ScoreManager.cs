﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public Text winText;
    public Text winTextS;
    // Use this for initialization
    void Start()
    {
        winText.text = "";
        winTextS.text = "";
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayBeep();
        }
    }

    private void PlayBeep()
    {
        gameObject.GetComponent<AudioSource>().Play();
        winText.text = "You Win!";
        winTextS.text = "You Win!";
    }

}
