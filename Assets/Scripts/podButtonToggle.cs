﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class podButtonToggle : MonoBehaviour {

    private Light buttonLight;
    private bool podActive = false;
    private int numberOfEntities;

    public GameObject bridge;
    public float speed;

    public GameObject bridgeStartLocation;
    public GameObject bridgeActiveLocation;

    void Start()
    {
        buttonLight = GetComponent<Light>();
    }


    private void FixedUpdate()
    {
        if (podActive)
        {
            bridge.transform.position = Vector3.Lerp(bridge.transform.position, bridgeActiveLocation.transform.position, speed * Time.deltaTime);
        }
        else
        {
            bridge.transform.position = Vector3.Lerp(bridge.transform.position, bridgeStartLocation.transform.position, speed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        podActive = !podActive;
    }



}

