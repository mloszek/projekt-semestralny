﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onDestro : MonoBehaviour {

    public GameObject objectToSpawn;

    private void OnDestroy()
    {
        Object.Instantiate(objectToSpawn, transform.position, transform.rotation);
    }

}
