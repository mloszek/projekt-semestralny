﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class welcomeText : MonoBehaviour {

    public Text initialText;
	// Use this for initialization
	void Start () {
        initialText.text =
            "Controlls: Arrows to move, Space to jump, " +
            "RMB and LMB to use abilities" +
            "Press Fire to hide tihs text.";
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1")) initialText.text = "";
    }
}
