﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gravity : MonoBehaviour
{
    private bool firePressed = false;
    private List<Rigidbody> objectsInRange = new List<Rigidbody>();

    public float forceIncreaseMultiplier; //modifies rate of increasing force applied to selected item
    public float forceDecreaseMultiplier; //modifies rate of decreasing force applied to selected item
    public float maxForce;
    //private AudioSource audioBuzz;


    public GameObject particleContainer;

    [SerializeField]
    private float force;

   /* private void Start()
    {
        AudioSource audioBuzz = GetComponent<AudioSource>();
    }*/

    void Update () {
        //capture fire input to bool. Based on this execute force calculations
        if (Input.GetButtonDown("Fire2")) firePressed = true;
        if (Input.GetButtonUp("Fire2")) firePressed = false;
        //call 
        if (firePressed)
        {
            IncreaseForce();
            particleContainer.SetActive(true);
            //audioBuzz.Play();
        }
        else if (!firePressed && force >= 0)
        {
            DecreaseForce();
            particleContainer.SetActive(false);
            //audioBuzz.Stop();
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetButton("Fire2")) //apply force opposite to gravity to all objects in range
        {
            foreach (Rigidbody interactable in objectsInRange)
            {
                interactable.AddForce(Vector3.up * force);
            }
        }
    }

    //Add and remove interactable objets from list if thay are in range.
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Magnet") objectsInRange.Add(other.GetComponent<Rigidbody>());
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Magnet") objectsInRange.Remove(other.GetComponent<Rigidbody>());
    }

    //Modify force which is applied to objects here
    void IncreaseForce() //force applied to objects increases
    {
        if (force < maxForce) force = force + Time.deltaTime * forceIncreaseMultiplier;
    }
    void DecreaseForce() //force applied to objects decreases
    {
        force = force - Time.deltaTime * forceDecreaseMultiplier;
        if (force < 0) force = 0; //to avoid negative force
    }


}
