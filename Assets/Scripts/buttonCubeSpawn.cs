﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonCubeSpawn : MonoBehaviour {

    private float timer;
    private Light buttonLight;

    public float buttonActivationDelay;
    public GameObject cublet;

    public GameObject spawn1;
    public GameObject spawn2;
    public GameObject spawn3;

    void Start()
    {
        buttonLight = GetComponent<Light>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (timer > buttonActivationDelay)
        {
            timer = 0;
            buttonLight.enabled = false;
            Object.Instantiate(cublet, spawn1.transform.position, spawn1.transform.rotation);
            Object.Instantiate(cublet, spawn2.transform.position, spawn1.transform.rotation);
            Object.Instantiate(cublet, spawn3.transform.position, spawn1.transform.rotation);
            PlayBeep();
        }
    }

	
	// Update is called once per frame
	void Update () {
        timer = timer + Time.deltaTime;
        if (timer > buttonActivationDelay) buttonLight.enabled = true;
	}

    private void PlayBeep()
    {
        gameObject.GetComponent<AudioSource>().Play();
    }
}
