﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugSpawn : MonoBehaviour {

    public GameObject player;

    public GameObject startSpawn;
    public GameObject liftObject;
    public GameObject arena;
    public GameObject arena2;
    public GameObject buttons;
    public GameObject jumping;
    public GameObject explore;

    void Update () {
        if (Input.GetKey(KeyCode.F1)) { player.transform.position = startSpawn.transform.position;    }
        if (Input.GetKey(KeyCode.F2)) { player.transform.position = liftObject.transform.position;    }
        if (Input.GetKey(KeyCode.F3)) { player.transform.position = arena.transform.position;          }
        if (Input.GetKey(KeyCode.F4)) { player.transform.position = arena2.transform.position;        }
        if (Input.GetKey(KeyCode.F5)) { player.transform.position = buttons.transform.position;      }
        if (Input.GetKey(KeyCode.F6)) { player.transform.position = jumping.transform.position;     }
        if (Input.GetKey(KeyCode.F7)) { player.transform.position = explore.transform.position;     }
    }
}
