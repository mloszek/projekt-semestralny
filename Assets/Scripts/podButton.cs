﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class podButton : MonoBehaviour {

    private Light buttonLight;
    private bool podActive = false;
    private int numberOfEntities;

    public GameObject bridge;
    public float speed;

    public GameObject bridgeStartLocation;
    public GameObject bridgeActiveLocation;

    //AudioSource beeper;

    void Start()
    {
        buttonLight = GetComponent<Light>();
       // beeper = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (numberOfEntities > 0) podActive = true;
        else podActive = false;
    }

    private void FixedUpdate()
    {
        if (podActive)
        {
            buttonLight.enabled = false;
            bridge.transform.position = Vector3.Lerp(bridge.transform.position, bridgeActiveLocation.transform.position, speed * Time.deltaTime);
            PlayBeep();
        }
        else
        {
            buttonLight.enabled = true;
            bridge.transform.position = Vector3.Lerp(bridge.transform.position, bridgeStartLocation.transform.position, speed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        numberOfEntities++;
    }

    private void OnCollisionExit(Collision collision)
    {
        numberOfEntities--;
        
    }

    private void PlayBeep()
    {
        gameObject.GetComponent<AudioSource>().Play();
    }

}

