﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour {

    private Vector3 dir;
    private Rigidbody rb;
    private bool firePressed;
    
    public GameObject particle;

    public float forceIncreaseMultiplier; //modifies rate of increasing force applied to selected item
    public float forceDecreaseMultiplier; //modifies rate of decreasing force applied to selected item
    public float maxForce;

    [SerializeField]
    private float force;


    void Update () {
        //capture fire input to bool. Based on this execute force calculations
        if (Input.GetButtonDown("Fire1")) firePressed = true;
        if (Input.GetButtonUp("Fire1")) firePressed = false;

        //call 
        if (firePressed)
        {
            particle.SetActive(true);

        }
        else if (!firePressed && force >= 0)
        {
            DecreaseForce();
            particle.SetActive(false);
        } 
    }

    void FixedUpdate()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 100;
        Debug.DrawRay(transform.position, forward, Color.green);
        if (Input.GetButton("Fire1")) //if player holds FIRE, apply force to direction of the player.
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.rigidbody != null)
                {
                    if (force < maxForce) IncreaseForce();

                    dir = transform.position - hit.transform.position;
                    //dir = dir.normalized;
                    hit.rigidbody.AddForce(dir * force);
                   // hit.rigidbody.AddForce(ray.origin * force);
                }
            }
        }
    }

    void IncreaseForce() //force applied to objects increases
    {
        force = force + Time.deltaTime * forceIncreaseMultiplier;
    }

    void DecreaseForce() //force applied to objects decreases
    {
        force = force - Time.deltaTime * forceDecreaseMultiplier;
        if (force < 0) force = 0; //to avoid negative force
    }


}
