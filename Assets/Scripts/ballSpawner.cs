﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballSpawner : MonoBehaviour {

    public GameObject objectToSpawn;

    public GameObject spawnpoint1;
    public GameObject spawnpoint2;
    public GameObject spawnpoint3;
    public GameObject spawnpoint4;
    public GameObject spawnpoint5;
    public GameObject spawnpoint6;
    public GameObject spawnpoint7;
    public GameObject spawnpoint8;
    public GameObject spawnpoint9;
    public GameObject spawnpoint10;

    public float delayBetweenWaves;
    private float time;

    private GameObject[] spawnPoints = new GameObject[10];

    void Start()
    {
        spawnPoints[0] = spawnpoint1;
        spawnPoints[1] = spawnpoint2;
        spawnPoints[2] = spawnpoint3;
        spawnPoints[3] = spawnpoint4;
        spawnPoints[4] = spawnpoint5;
        spawnPoints[5] = spawnpoint6;
        spawnPoints[6] = spawnpoint7;
        spawnPoints[7] = spawnpoint8;
        spawnPoints[8] = spawnpoint9;
        spawnPoints[9] = spawnpoint10;
    }

    private void Update()
    {
        time = time + Time.deltaTime;
        if (time > delayBetweenWaves)
        {
            StartCoroutine(Spawner());
            time = 0;
        }
    }

    IEnumerator Spawner()
    {
        foreach (GameObject spawnObject in spawnPoints)
        {
            Object.Instantiate(objectToSpawn, spawnObject.transform.position, spawnObject.transform.rotation);
            yield return new WaitForSeconds(1);
        }
        
    }
}
