﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endButton : MonoBehaviour
{

    public GameObject myPlayer;
    public GameObject movetoMap;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            myPlayer.transform.position = movetoMap.transform.position;
            myPlayer.transform.rotation = movetoMap.transform.rotation;
            PlayBeep();
        }
    }

    private void PlayBeep()
    {
        gameObject.GetComponent<AudioSource>().Play();
    }
}
