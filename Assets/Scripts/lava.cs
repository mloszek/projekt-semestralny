﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lava : MonoBehaviour {

    public GameObject myPlayer;
    public GameObject respawnPoint;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Magnet") Destroy(collision.gameObject, 1);
        if (collision.gameObject.tag == "Player") myPlayer.transform.position = respawnPoint.transform.position;
    }
}
